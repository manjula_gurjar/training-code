/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */

/** @var $block BC\Employee\Block\Add */
var config = {
   map: {
       "*": {
           phone_number_ten_digit: "BC_Employee/js/phoneNumberValidationRule"
       }
   }
};