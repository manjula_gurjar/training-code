/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */

/** @var $block BC\Employee\Block\Add */
define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
    ], function($){
        'use strict';
        return function() {
            $.validator.addMethod(
                "phone_number_ten_digit",
                function(value, element) {
                   return value.length > 9 &&  value.length < 11 
                   && value.match(/^\d{10}$/);
                },
                $.mage.__("Please specify a valid 10 digits mobile number")
            );
    }
});