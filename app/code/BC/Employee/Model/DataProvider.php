<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Model;

use BC\Employee\Model\ResourceModel\Employee\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $employeeCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param $meta
     * @param $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $employeeCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        array $meta = [],
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->collection = $employeeCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $employee) {
            $this->loadedData[$employee->getId()] = $employee->getData();
            $empData = $employee->getData();
            if (isset($empData['profile_image'])) {
                $m['profile_image'][0]['name'] = $empData['profile_image'];
                $m['profile_image'][0]['url'] = $this->getImageUrl($empData['profile_image']);
                $fullData = $this->loadedData;
                $this->loadedData[$employee->getId()] = array_merge($fullData[$employee->getId()], $m);
            }        
        }
        return $this->loadedData;
    }
    /**
     * get image url
     * @return string
     */
    public function getImageUrl($imageName)
    {
        $firstName = substr($imageName, 0, 1);
        $secondName = substr($imageName, 1, 1);
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'profile/image/' . $firstName . '/' . $secondName . '/' . $imageName;
        return $mediaUrl;
    }
}