<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Model\ResourceModel\Employee;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use BC\Employee\Model\Employee as EmployeeModel;
use BC\Employee\Model\ResourceModel\Employee as EmployeeResourceModel;

class Collection extends AbstractCollection
{
	/**
     * @param EmployeeModel
     * @param EmployeeResourceModel
     */
    protected function _construct()
    {
        $this->_init(EmployeeModel::class, EmployeeResourceModel::class);
    }
}