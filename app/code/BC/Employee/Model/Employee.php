<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Model;

use Magento\Framework\Model\AbstractModel;
use BC\Employee\Model\ResourceModel\Employee as EmployeeResourceModel;

class Employee extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(EmployeeResourceModel::class);
    }
}