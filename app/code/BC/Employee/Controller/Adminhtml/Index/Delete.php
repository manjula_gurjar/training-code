<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

class Delete extends Action
{
    /**
     * @var \BC\Employee\Model\Employee
     */
    protected $modelEmployee;
    /**
     * @param Context $context
     * @param \BC\Employee\Model\Employee $employeeFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \BC\Employee\Model\Employee $employeeFactory
    ) {
        parent::__construct($context);
        $this->modelEmployee = $employeeFactory;
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->modelEmployee;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Employee deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Employee does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }
}