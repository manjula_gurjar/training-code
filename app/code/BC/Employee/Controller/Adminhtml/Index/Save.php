<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use BC\Employee\Model\Employee;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var Employee
     */
    protected $uiEmployeemodel;
    /**
     * @var Session
     */
    protected $adminsession;
    /**
     * @param Action\Context $context
     * @param Employee       $uiEmployeemodel
     * @param Session        $adminsession
     */
    public function __construct(
        Action\Context $context,
        Employee $uiEmployeemodel,
        Session $adminsession
    ) {
        parent::__construct($context);
        $this->uiEmployeemodel = $uiEmployeemodel;
        $this->adminsession = $adminsession;
    }
    /**
     * Save Employee record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $employee_id = $this->getRequest()->getParam('id');
            if ($employee_id) {
                $this->uiEmployeemodel->load($employee_id);
            }
            $data = $this->_filterEmployeeData($data);
            $this->uiEmployeemodel->setData($data);
            try {
                $this->uiEmployeemodel->save();
                $this->messageManager->addSuccess(__('The employee has been saved.'));
                $this->adminsession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    if ($this->getRequest()->getParam('back') == 'add') {
                        return $resultRedirect->setPath('*/*/add');
                    } else {
                        return $resultRedirect->setPath('*/*/edit', ['id' => $this->uiEmployeemodel->getId(), '_current' => true]);
                    }
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    /**
     * Replace image array with name
     *
     * @return array
     */
    public function _filterEmployeeData(array $rawData)
    {
        $data = $rawData;
        if (isset($data['profile_image'][0]['name'])) {
            $data['profile_image'] = $data['profile_image'][0]['name'];
        } else {
            $data['profile_image'] = null;
        }
        return $data;
    }
}
