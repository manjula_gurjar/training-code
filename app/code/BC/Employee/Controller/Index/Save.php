<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use BC\Employee\Model\Employee;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Employee
     */
    protected $employeeModel;
    /**
     * File uploader
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;
    /**
     * @param Action\Context                      $context
     * @param Employee                            $employeeModel
     * @param \Magento\Framework\Filesystem       $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        Context $context,
        \BC\Employee\Model\EmployeeFactory $employeeModel,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
    ) {
        parent::__construct($context);
        $this->employeeModel = $employeeModel;
        $this->filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
    }
    /**
     * Save Employee record action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $newFileName = null;
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $files = $this->getRequest()->getFiles();
            if(isset($files['profile_image']['name']) && $files['profile_image']['name']){
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'profile_image']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);

                $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('profile/image/');
                $result = $uploader->save($path);
                $newFileName = $result['file'];
            }
            $employee_id = $this->getRequest()->getParam('id');
            $eModel = $this->employeeModel->create();
            if ($employee_id) {
                $eModel->load($employee_id);
                if($newFileName || isset($data['is_delete'])) {
                    $data = $this->_filterEmployeeData($data, $newFileName);
                }
            } else{
                $data = $this->_filterEmployeeData($data, $newFileName);
            }
            $eModel->setData($data);
            try {
                if ($employee_id) {
                    $eModel->setId($employee_id);
                }
                $eModel->save();
                $this->messageManager->addSuccess(__('The employee has been saved.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * function to add image name in data to save in database
     *
     * @return array
     */
    public function _filterEmployeeData(array $rawData, $newName)
    {
        $data = $rawData;
        if ($newName) {
            $data['profile_image'] = substr($newName, strrpos($newName, '/') + 1);
        } else {
            $data['profile_image'] = null;
        }
        return $data;
    }
}