<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Controller\Index;

class Delete extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \BC\Employee\Model\Employee
     */
    protected $modelEmployee;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \BC\Employee\Model\Employee $employeeFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \BC\Employee\Model\Employee $employeeFactory
    ) {
        parent::__construct($context);
        $this->modelEmployee = $employeeFactory;
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->modelEmployee;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Employee deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->messageManager->addError(__('Employee does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }
}