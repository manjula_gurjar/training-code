<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Block;

use BC\Employee\Model\Employee;

class Edit extends \Magento\Framework\View\Element\Template
{
    /**
    * @var \BC\Employee\Model\EmployeeFactory
    */
    protected $_employeeFactory;

    /* @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \BC\Employee\Model\EmployeeFactory $employeeFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->_employeeFactory = $employeeFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    /**
     * Get form action URL for POST employee add request
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('employee/index/save',['id'=>$this->getRequest()->getParam('id')]);
    }

    /**
     * Get current employee data
     *
     * @return array
     */
    public function getEmployeeData()
    {
        $id = $this->getRequest()->getParam('id');
        $empData = $this->_employeeFactory->create()->load($id);
        return $empData;
    }

    /**
     * Get media url
     *
     * @return array
     */
    public function getMediaUrl()
    {
        $storeManager = $this->_storeManager->getStore();
        $mediaUrl = $storeManager->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }

    /**
     * Get image url
     *
     * @return array
     */
    public function getImageUrl($imageName)
    {
        $firstName = substr($imageName, 0, 1);
        $secondName = substr($imageName, 1, 1);
        return $this->getMediaUrl() . 'profile/image/' . $firstName . '/' . $secondName . '/'. $imageName;
    }
}