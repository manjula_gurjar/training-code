<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Block;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use BC\Employee\Model\EmployeeFactory;
use Magento\Store\Model\StoreManagerInterface;

class Employee extends Template
{
    /**
    * @var \BC\Employee\Model\EmployeeFactory
    */
    protected $_employeeFactory;

    /* @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
    * @param Template\Context $context
    * @param EmployeeFactory $employeeFactory
    * @param StoreManagerInterface $storeManager
    */
    public function __construct(Context $context,EmployeeFactory $employeeFactory,StoreManagerInterface $storeManager,array $data = [])
    {
        $this->_employeeFactory = $employeeFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $data);
    }
    /**
     * Set employee collection
     */
    protected  function _construct()
    {
        parent::_construct();
        $collection = $this->_employeeFactory->create()->getCollection()
            ->setOrder('id', 'DESC');
        $this->setCollection($collection);
    }

   /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
           'Magento\Theme\Block\Html\Pager',
           'employee.list.pager'
        );
        $pager->setLimit(6)
            ->setShowAmounts(true)
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();

        return $this;
    }

   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get media url
     *
     * @return array
     */
    public function getMediaUrl()
    {
        $storeManager = $this->_storeManager->getStore();
        $mediaUrl = $storeManager->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }

    /**
     * Get profile image url
     *
     * @return string
     */
    public function getImageUrl($imageName)
    {
        if($imageName) {
            $firstName = substr($imageName, 0, 1);
            $secondName = substr($imageName, 1, 1);
            return $this->getMediaUrl() . 'profile/image/' . $firstName . '/' . $secondName . '/'. $imageName;
        } else {
            return $this->getViewFileUrl('BC_Employee::images/user-placeholder.png');
        }
    }
}