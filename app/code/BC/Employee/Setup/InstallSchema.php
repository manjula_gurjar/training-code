<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Created By : Manjula Gurjar
 */
namespace BC\Employee\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        /**
         * Create table 'employee'
         */
        if (!$installer->tableExists('employee')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('employee')
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true,
                ],
                'ID'
            )->addColumn(
                'profile_image',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                ],
                'Profile Image'
            )->addColumn(
                'first_name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                ],
                'First Name'
            )->addColumn(
                'middle_name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => true',
                ],
                'Middle Name'
            )->addColumn(
                'last_name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                ],
                'Last Name'
            )->addColumn(
                'dob',
                Table::TYPE_DATE,
                255,
                [
                    'nullable => false',
                ],
                'Date of Birth'
            )->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                ],
                'Employee email'
            )->addColumn(
                'phone',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                    'default => 0',
                ],
                'Employee Phone number'
            )->addColumn(
                'father_name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable => false',
                ],
                'Father Name'
            )->addColumn(
                'designation',
                Table::TYPE_TEXT,
                '255',
                [
                    'nullable => true'
                ],
                'Designation'
            )->addColumn(
                'address',
                Table::TYPE_TEXT,
                '255',
                [
                    'nullable => true'
                ],
                'Employee Address'
            )->addColumn(
                'salary',
                Table::TYPE_DECIMAL,
                '8,2',
                [
                    'nullable => true'
                ],
                'Salary'
            )->addColumn(
                'date_of_joining',
                Table::TYPE_DATE,
                null,
                [ 
                    'nullable => true'
                ],
                'Date of Joining'
            )->setComment('Employee Table');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}